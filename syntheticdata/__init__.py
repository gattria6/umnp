import datetime
import random


class SyntheticDatum:
    def __init__(self):
        self.__fields = []

    def get_datum(self, sep=", "):
        return sep.join(self.__fields)

    def add_value(self, value):
        self.__fields.append(str(value))

    def add_values(self, values: list | tuple):
        for field in values:
            self.__fields.append(str(field))

    def add_time(self, ts: datetime.datetime | None = None):
        if ts is None:
            ts = datetime.datetime.now(tz=datetime.UTC)
        ts = round_to_second(ts)
        self.__fields.append(str(ts.strftime("%H:%M:%S")))

    def add_date(self, ts: datetime.datetime | None = None, fmt="%Y-%m-%d"):
        if ts is None:
            ts = datetime.datetime.now(tz=datetime.UTC)
        ts = round_to_second(ts)
        self.__fields.append(str(ts.strftime(fmt)))

    def add_integer(
        self,
        min_val: int = 0,
        max_val: int = 999,
        count=1,
    ):

        for i in range(count):
            value = random.randint(min_val, max_val)
            self.__fields.append(str(value))

    def add_float(
        self, min_val: float = 0.0, max_val: float = 1.0, fmt: str = "0.2f", count=1
    ):

        for i in range(count):
            value = random.uniform(min_val, max_val)
            self.__fields.append(f"{value:{fmt}}")

    def add_room_temperature(self, fmt: str = "0.2f", count=1):
        for i in range(count):
            value = random.uniform(15, 20)
            self.__fields.append(f"{value:{fmt}}")

    def add_pressure_pa(self, fmt: str = ".0f", count=1):
        for i in range(count):
            value = random.uniform(100000 * 0.95, 100000 * 1.05)
            self.__fields.append(f"{value:{fmt}}")


def round_to_second(when: datetime.datetime | None = None) -> datetime.datetime:

    if when is None:
        when = datetime.datetime.now(tz=datetime.timezone.utc)
    if not isinstance(when, datetime.datetime):
        raise ValueError(f"Timestamp {when} is not a datetime.datetime")
    if when.microsecond >= 500 * 1000:
        when = when + datetime.timedelta(seconds=1)
    now = when.replace(microsecond=0)
    return now
