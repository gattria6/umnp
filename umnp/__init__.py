import sys

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import machine
else:
    from umnp.microcontroller.umock import machine

UNIQUE_ID_MAC = machine.unique_id()[:6]
UNIQUE_ID_MAC_STR = ':'.join('%02x' % x for x in UNIQUE_ID_MAC)
