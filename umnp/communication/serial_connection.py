import asyncio
import logging
import threading
import time

import serial

from umnp.communication import ConnectionProblemException, SERIAL_DEFAULT_BAUD_RATE, SERIAL_DEFAULT_WAIT_TIME_S
from umnp.communication.abstract_serial_connection import AbstractSerialConnection


class SerialConnection(AbstractSerialConnection):
    def __init__(self, address, options=None):
        super().__init__(address, options)
        self.connect()
        self.__lock = threading.RLock()

    @property
    def baud_rate(self):
        return self.get_option("baudrate", SERIAL_DEFAULT_BAUD_RATE)

    def connect(self):
        if self.connection:
            return
        attempts = 0
        connection = None
        max_attempts = self.max_connection_attempts
        while attempts < max_attempts:
            try:
                connection = serial.Serial(
                    port=self.address,
                    baudrate=self.baud_rate,
                    parity=self.get_option("parity", serial.PARITY_NONE),
                    stopbits=self.get_option("stopbits", serial.STOPBITS_ONE),
                    timeout=self.get_option("wait time", SERIAL_DEFAULT_WAIT_TIME_S),
                    xonxoff=self.get_option("xonoff", False),
                    rtscts=self.get_option("rtscts", False),
                    dsrdtr=self.get_option("dsrdtr", False),
                )
            except serial.serialutil.SerialException as e:
                raise ConnectionProblemException(e)

            if connection:
                break

            attempts += 1
            time.sleep(0.1)

        if not connection:
            print(f"could not connect to {self.address}")
            return

        time.sleep(0.1)

        self.connection = connection
        connection.flush()
        connection.reset_input_buffer()
        connection.reset_output_buffer()
        print(f"connected to {self.address}, baud rate: {self.baud_rate}")
        logging.debug(connection)

    def sync_command(self, cmd, expected_lines=1, show_reply=True):
        if not self.connection:
            logging.error(f"could not send command '{cmd}': not connected")
            return None
        time.sleep(self.wait_time)
        self.connection.flushInput()
        self.connection.flushOutput()

        if isinstance(self.line_sep_read, bytes):
            empty = self.line_sep_read.decode('utf8')
        elif isinstance(self.line_sep_read, str):
            empty = self.line_sep_read
        else:
            empty = ""

        result = ''
        result_debug = "<no response>"
        cmd_nice = cmd
        print(
            f"sending sync command '{cmd_nice}' to '{self.address}' and expecting {expected_lines} lines of results")

        with self.__lock:
            self.send_command(cmd)
            # time.sleep(self.wait_time)

            if expected_lines == 1:
                result = self.read_line(timeout=self.wait_time)
            else:
                received_lines = 0
                while received_lines < expected_lines:
                    line = self.read_line(timeout=self.wait_time)
                    if line and line != empty:
                        result += line
                        received_lines += 1

        if result:
            if isinstance(result, bytes):
                result = result.decode("utf8")
                result_debug = result.replace('\n', '\\n').replace('\r', '\\r')
            else:
                result_debug = result.replace('\n', '\\n').replace('\r', '\\r')

        if show_reply:
            print(f"received (processed) '{result_debug}' from {self.address} in response to '{cmd}'")
        else:
            print(f"received (processed) '{result_debug}' from {self.address} in response to '{cmd}'")
        return result

    def read_line(self, timeout=None):
        # fixme: what do we do if we have no connection
        if not self.connection:
            return None

        result = b''
        # fixme: what do we do on an error?

        with self.__lock:
            sep = self.line_sep_read
            sep = None
            if sep:
                start_time = time.time()
                try:
                    while True:
                        newest_char = self.connection.read(1)
                        print(newest_char)
                        if newest_char is not None:
                            result += newest_char
                        if newest_char == sep:
                            break
                        if time.time() - start_time > timeout:
                            # break
                            pass
                except serial.serialutil.SerialException:
                    pass
            else:
                try:
                    result = self.connection.readline()
                    print("Result:", result.decode('utf-8').strip())
                except serial.serialutil.SerialException:
                    pass

        logging.debug(f"received (raw) '{result}' from {self.address}")

        return result.decode('utf8').strip()

    def send_command(self, cmd):
        if not self.connection:
            logging.error(f"could not send command '{cmd}': not connected")
            return None

        print(f"sending command '{cmd}' to '{self.address}'")

        if isinstance(cmd, str):
            cmd = cmd.encode("utf-8")

        if self.line_sep_write:
            sep = self.line_sep_write
            if isinstance(sep, str):
                sep = sep.encode('utf-8')

            cmd += sep

        with self.__lock:
            self.connection.write(cmd)

