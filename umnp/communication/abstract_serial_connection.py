import asyncio

from umnp.communication import SERIAL_DEFAULT_BAUD_RATE, SERIAL_DEFAULT_WAIT_TIME_S


class AbstractSerialConnection:
    def __init__(self, address, options: dict | None = None):
        self.__connection = None

        self.__address = address
        if options is None:
            self.__options = {}
        else:
            self.__options = options

        self.__baud_rate = self.get_option("baudrate", SERIAL_DEFAULT_BAUD_RATE)
        self.__wait_time = self.get_option("wait time", SERIAL_DEFAULT_WAIT_TIME_S)
        self.__line_sep_read = self.get_option("read separator", b"\r")
        self.__line_sep_write = self.get_option("write separator", b"\r")
        self.__max_connection_attempts = self.get_option("max connection attempts", 3)



    @property
    def connection(self):
        return self.__connection

    @property
    def max_connection_attempts(self):
        return self.__max_connection_attempts
    @property
    def address(self):
        return self.__address
    @property
    def wait_time(self):
        return self.__wait_time
    @property
    def line_sep_read(self):
        return self.__line_sep_read
    @property
    def line_sep_write(self):
        return self.__line_sep_write

    @connection.setter
    def connection(self, conn):
        self.__connection = conn


    def connect(self):
        raise NotImplementedError

    def disconnect(self):
        raise NotImplementedError

    def sync_command(self, command, expected_lines=None, show_reply=False):
        raise NotImplementedError

    def send_command(self, command):
        raise NotImplementedError

    def read_line(self, timeout=None):
        raise NotImplementedError

    def get_option(self, name: str, default=None):
        if self.__options is None:
            return default
        return self.__options.get(name, default)
