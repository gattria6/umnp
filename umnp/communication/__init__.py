SERIAL_DEFAULT_BAUD_RATE = 115200
SERIAL_DEFAULT_WAIT_TIME_S = 0.5


class ConnectionProblemException(Exception):
    pass
