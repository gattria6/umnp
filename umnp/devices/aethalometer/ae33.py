from umnp.communication.abstract_serial_connection import AbstractSerialConnection

AE33_BAUDRATE = 115200


class SerialConnection:
    def __init__(self):
        pass


class AE33:
    def __init__(self, connection: AbstractSerialConnection):
        self.__last_measurement = None
        self.__connection = connection
        pass

    def request_measurement(self) -> str | None:
        return self.__connection.sync_command(b"$AE33:D1\r", 1)

    def current_measurement(self) -> str | None:
        current = self.request_measurement()
        if current == self.__last_measurement:
            return None
        self.__last_measurement = current
        return current
