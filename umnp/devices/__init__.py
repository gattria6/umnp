DEVICE_TYPE_UNKNOWN = 0
DEVICE_TYPE_RHTP = 1
DEVICE_TYPE_TEST = 777


DEVICE_TYPE_NAMES = {
    DEVICE_TYPE_RHTP: "RHTP",
    DEVICE_TYPE_UNKNOWN: "unknown",
    DEVICE_TYPE_TEST: "test device",
}


def get_device_type_name(device_type: int) -> str:
    return DEVICE_TYPE_NAMES.get(device_type, "invalid-device")
