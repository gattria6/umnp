import datetime
import os

from umnp.devices import get_device_type_name
from umnp.proto import DataMessage, DeviceMessage
from umnp.proto.headers import DEVICE_HEADERS
from umnp.proto.message import Message


def clean(field: str) -> str:
    return field.replace(",", ";")


class FileDAQ:
    def __init__(self, output_dir: str = "daq-data"):
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)
        self.__output_dir = output_dir
        self.__open_files = {}
        self.__open_fns = {}

    def filename(self, msg: Message, today: str):
        device_type_name = get_device_type_name(msg.sender_type)
        message_type_name = msg.get_message_type_name()
        ident = f"{today}-{device_type_name}-{msg.sender_id}-{message_type_name}"
        fn = os.path.join(self.__output_dir, f"{ident}.csv")
        return fn, ident, today

    def start_file(
        self, msg: Message, when: datetime.datetime, file_id: str, filename: str
    ):
        self.__open_files[file_id] = open(filename, "w")
        f = self.__open_files[file_id]
        self.__open_fns[file_id] = filename
        headers = ["receive-time", "sender-id"]
        if isinstance(msg, DataMessage):
            headers.extend(DEVICE_HEADERS.get(msg.sender_type, {}).get(msg.type))

        elif isinstance(msg, DeviceMessage):
            headers.extend(msg.header_fields)

        else:
            headers = ["# unknown"]
        headers = [header.replace(",", ";") for header in headers]
        header = ",".join(headers)
        if header:
            f.write(header + "\n")
        return f

    def save_message(self, msg: Message):
        msg_type = msg.type
        when = datetime.datetime.now(tz=datetime.timezone.utc)
        today = when.strftime("%Y-%m-%d")
        filename, file_id, today = self.filename(msg, today)
        f = None
        if file_id in self.__open_fns:

            if self.__open_fns[file_id] == filename:
                f = self.__open_files[file_id]
            else:
                # date has changed in the meantime
                f = self.__open_files[file_id]
                f.close()
                f = self.start_file(msg, when, file_id, filename)

        else:
            if (
                os.path.exists(filename)
                and self.__open_files.get(file_id, None) is None
            ):
                self.__open_files[file_id] = open(filename, "a")
                self.__open_fns[file_id] = filename
                f = self.__open_files[file_id]
            else:
                f = self.start_file(msg, when, file_id, filename)
                f.flush()

        if f:
            if isinstance(msg, DataMessage):
                payload = clean(msg.payload())
                f.write(f"{when},{msg.sender_id},{payload}\n")
            elif isinstance(msg, DeviceMessage):
                payload = clean(msg.payload())
                msg_type = clean(msg.get_message_type_string)
                f.write(f"{when},{msg.sender_id},{msg_type},{payload}\n")
            else:
                f.write(msg.payload() + "\n")
            f.flush()
