from umnp.proto.common.logging import log_error
from umnp.proto.common.timestamp import TimeStamp, valid_timestamp
from umnp.proto.constants import (
    MSG_PROTOCOL_VERSION,
    MSG_LEN_PROTOCOL_VERSION,
    MSG_BYTE_ORDER,
    MSG_LEN_MESSAGE_TYPE,
    MSG_LEN_TIMESTAMP,
    MSG_LEN_SENDER_ID,
    MSG_LEN_SENDER_TYPE,
)

try:
    # noinspection PyUnresolvedReferences
    import typing
except ImportError:
    pass


class MessageHeader:
    def __init__(
        self,
        msg_type: int,
        sender_device_id: bytes,
        sender_device_type: int,
        send_time: typing.Optional[TimeStamp] = None,
        version: int = MSG_PROTOCOL_VERSION,
    ):
        self._message_type = msg_type
        self._send_time = valid_timestamp(send_time)
        self._timestamp = TimeStamp(self._send_time)
        self._version = version
        self._sender_device_id = sender_device_id
        self._sender_dev_type = sender_device_type

    @property
    def timestamp_encoded(self) -> bytes:
        return self._timestamp.value.to_bytes(MSG_LEN_TIMESTAMP, MSG_BYTE_ORDER)

    @property
    def sender_dev_id(self) -> bytes:
        return self._sender_device_id

    @property
    def message_type(self) -> int:
        return self._message_type

    @property
    def message_type_encoded(self) -> bytes:
        return self.message_type.to_bytes(MSG_LEN_MESSAGE_TYPE, MSG_BYTE_ORDER)

    @property
    def version(self) -> int:
        return self._version

    @property
    def version_encoded(self) -> bytes:
        return self._version.to_bytes(MSG_LEN_PROTOCOL_VERSION, MSG_BYTE_ORDER)

    @property
    def sender_id(self) -> bytes:
        return self._sender_device_id

    @property
    def sender_type(self) -> int:
        return self._sender_dev_type

    @property
    def sender_dev_type_encoded(self) -> bytes:
        return self._sender_dev_type.to_bytes(MSG_LEN_SENDER_TYPE, MSG_BYTE_ORDER)

    @property
    def timestamp(self) -> TimeStamp:
        return self._timestamp

    @property
    def sender_device_id_encoded(self) -> bytes:
        if len(self._sender_device_id) != 6:
            raise ValueError("Device ID length != 6 bytes")
        return self._sender_device_id

    def encode(self) -> bytes:
        version = self.version_encoded
        sender_id = self.sender_device_id_encoded
        sender_type = self.sender_dev_type_encoded
        message_type = self.message_type_encoded
        timestamp = self.timestamp_encoded
        return version + sender_id + sender_type + message_type + timestamp

    @property
    def encoded_size_bytes(self) -> int:
        return (
            MSG_LEN_PROTOCOL_VERSION
            + MSG_LEN_SENDER_ID
            + MSG_LEN_SENDER_TYPE
            + MSG_LEN_MESSAGE_TYPE
            + MSG_LEN_TIMESTAMP
        )

    @classmethod
    def decode(cls, data: bytes):
        offset = 0
        protocol_bytes = -1
        if not (isinstance(data, bytes)):
            log_error("Invalid message header: not bytes")
            return None
        try:
            protocol_bytes = data[:MSG_LEN_PROTOCOL_VERSION]
            protocol_version = int.from_bytes(protocol_bytes, MSG_BYTE_ORDER)

            cls._version = protocol_version
        except (KeyError, ValueError):
            log_error(f"Protocol bytes: {protocol_bytes}")
            log_error("Invalid message header: could not extract version")
            return None
        offset += MSG_LEN_PROTOCOL_VERSION

        try:
            msg_sender_id_b = data[offset : (offset + MSG_LEN_SENDER_ID)]
            offset += MSG_LEN_SENDER_ID
            msg_sender_type_b = data[offset : offset + MSG_LEN_SENDER_TYPE]
            offset += MSG_LEN_SENDER_TYPE
            msg_sender_type = int.from_bytes(msg_sender_type_b, MSG_BYTE_ORDER)
        except (KeyError, ValueError):
            log_error("Invalid message sender information: could not extract data")
            return None

        try:
            message_type_bytes = data[offset : offset + MSG_LEN_MESSAGE_TYPE]
            message_type = int.from_bytes(message_type_bytes, MSG_BYTE_ORDER)

        except (KeyError, ValueError):
            log_error("Invalid message payload: could not extract version")
            return None

        offset += MSG_LEN_MESSAGE_TYPE

        if protocol_version < 0 or protocol_version > MSG_PROTOCOL_VERSION:
            err = f"Invalid protocol version {protocol_version}, outside of range [0, {MSG_PROTOCOL_VERSION}]"
            log_error(err)
            return None
        message_ts = None
        try:
            message_ts = data[offset : offset + MSG_LEN_TIMESTAMP]
            message_ts_int = int.from_bytes(message_ts, byteorder=MSG_BYTE_ORDER)
            timestamp = TimeStamp(message_ts_int)
        except (KeyError, ValueError) as e:
            log_error(f"Invalid message timestamp: could not extract timestamp: {e}")
            return None

        return cls(
            message_type,
            sender_device_type=msg_sender_type,
            sender_device_id=msg_sender_id_b,
            version=protocol_version,
            send_time=timestamp,
        )
