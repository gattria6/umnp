from umnp.proto.data_message import DataMessage
from umnp.proto.device_message import DeviceMessage
from umnp.proto.messagetype import MessageType
from umnp.proto.register_messages import register_messages

register_messages(MessageType.MSG_DEVICE_DATA, DataMessage, "data")
register_messages(MessageType.MSG_DEVICE_INFO, DeviceMessage, "info")
