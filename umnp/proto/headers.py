from umnp.devices import DEVICE_TYPE_RHTP, DEVICE_TYPE_TEST
from umnp.proto.messagetype import MessageType

DEVICE_HEADERS = {
    DEVICE_TYPE_RHTP: {MessageType.MSG_DEVICE_DATA: ["T", "rH", "p", "T(p)"]},
    DEVICE_TYPE_TEST: {MessageType.MSG_DEVICE_DATA: ["message"]},
}
