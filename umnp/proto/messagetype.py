# This would be an enum, if micropython supported them


class MessageType:
    MSG_DEVICE_DATA = 1
    MSG_DEVICE_INFO = 16
    MSG_TYPE_ERROR = 32
    MSG_TYPE_UNKNOWN = 65535

    _allowed_message_types = [MSG_DEVICE_DATA, MSG_TYPE_UNKNOWN, MSG_DEVICE_INFO]

    @staticmethod
    def valid_message_type(msg_type: int):
        return msg_type in MessageType._allowed_message_types
