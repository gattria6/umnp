import sys

if sys.implementation.name == "micropython":
    def info(msg: str) -> None:
        print(f"INFO: {msg}")


    def error(msg: str) -> None:
        print(f"ERROR: {msg}")
else:
    import logging

    def log_info(msg: str) -> None:
        logging.info(msg)

    def log_error(msg: str) -> None:
        logging.error(msg)
