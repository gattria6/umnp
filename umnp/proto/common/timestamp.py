import sys

from umnp.proto.umnp_time import seconds_since_epoch

if sys.implementation.name == "micropython":
    pass
else:
    import datetime


def ts_utc_now_second() -> int:
    if sys.implementation.name == "micropython":
        # rtc = machine.RTC()
        # https://docs.micropython.org/en/latest/library/time.html#module-time
        # Micropython's time.time() returns the number of seconds, as an integer, since the Epoch
        return seconds_since_epoch()
    else:
        ts = datetime.datetime.now(tz=datetime.timezone.utc).timestamp()
        return int(round(ts))


class TimeStamp:
    def __init__(self, when=None):
        if when is None:
            self._ts = ts_utc_now_second()
        elif isinstance(when, TimeStamp):
            self._ts = when.value
        elif isinstance(when, int):
            self._ts = when
        else:
            error = f"Could not convert time {when} (type {type(when)} to TimeStamp."
            error += "Allowed values are integers > 0, TimeStamp objects or None."
            raise ValueError(error)

        if not isinstance(self._ts, int):
            raise ValueError("TimeStamp not an integer")
        if self._ts < 0:
            raise ValueError("TimeStamp < 0")

    @property
    def value(self) -> int:
        return self._ts


def valid_timestamp(when: TimeStamp | None) -> TimeStamp:
    if when is None:
        return TimeStamp()

    if not isinstance(when, TimeStamp):
        raise ValueError("Expected None or TimeStamp")

    return when
