try:
    # noinspection PyUnresolvedReferences
    import typing
except ImportError:
    pass
from umnp.proto.message import Message

MESSAGE_NAMES = {}


def register_messages(msg_type: int, msg: typing.Type[Message], name: str):
    msg.add_message_type(msg_type, msg, name)
