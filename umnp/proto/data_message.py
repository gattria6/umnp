from umnp.proto.common.timestamp import TimeStamp
from umnp.proto.constants import MSG_STRING_ENCODING
from umnp.proto.message import Message, MessageHeader
from umnp.proto.messagetype import MessageType


class DataMessage(Message):
    def __init__(
        self, data: str, sender_id: bytes, sender_type: int, send_time: TimeStamp = None
    ):
        self._payload = data
        self._encoded_data = data.encode(MSG_STRING_ENCODING)
        super().__init__(
            MessageType.MSG_DEVICE_DATA,
            self._encoded_data,
            sender_id,
            sender_type,
            send_time,
        )

    @staticmethod
    def _decode_payload(transferred_data):
        return transferred_data.decode(MSG_STRING_ENCODING)

    def payload(self) -> str:
        return self._payload

    @classmethod
    def decode(cls, payload: bytes, header: MessageHeader) -> "DataMessage":
        decoded_payload = cls._decode_payload(payload)
        return cls(
            decoded_payload, header.sender_id, header.sender_type, header.timestamp
        )
