from umnp.proto.common.timestamp import TimeStamp
from umnp.proto.constants import MSG_STRING_ENCODING
from umnp.proto.message import Message, MessageHeader, MSG_BYTE_ORDER
from umnp.proto.messagetype import MessageType

MSG_TYPE_UNKNOWN = 0
MSG_TYPE_ERROR = 1
MSG_TYPE_INFO = 2
MSG_TYPE_LIFE_SIGN = 3


_MSG_TYPE_DICT = {
    MSG_TYPE_ERROR: "error",
    MSG_TYPE_INFO: "info",
    MSG_TYPE_LIFE_SIGN: "seconds since boot",
}


class DeviceMessage(Message):
    def __init__(
        self,
        data: str,
        msg_type: int,
        sender_id: bytes,
        sender_type: int,
        send_time: TimeStamp = None,
    ):
        self._msg_type = msg_type
        self._payload = data
        _msg_type = msg_type.to_bytes(1, MSG_BYTE_ORDER)
        self._encoded_data = _msg_type + data.encode(MSG_STRING_ENCODING)
        super().__init__(
            MessageType.MSG_DEVICE_INFO,
            self._encoded_data,
            sender_id,
            sender_type,
            send_time,
        )

    @staticmethod
    def _decode_payload(transferred_data):
        return transferred_data.decode(MSG_STRING_ENCODING)

    @property
    def get_message_type_string(self):
        return _MSG_TYPE_DICT.get(self._msg_type, "unknown")

    @property
    def message_type(self):
        return self._msg_type

    def payload(self) -> str:
        return self._payload

    @classmethod
    def decode(cls, payload: bytes, header: MessageHeader) -> "DeviceMessage":
        decoded_payload = cls._decode_payload(payload[1:])
        return cls(
            decoded_payload,
            payload[0],
            header.sender_id,
            header.sender_type,
            header.timestamp,
        )

    @property
    def header_fields(self):
        return ["message-type", "message"]
