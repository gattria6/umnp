from umnp.proto.common.logging import log_error

try:
    # noinspection PyUnresolvedReferences
    import typing
except ImportError:
    pass

from umnp.proto.common.timestamp import TimeStamp
from umnp.proto.constants import MSG_BYTE_ORDER, MSG_LEN_PAYLOAD_SIZE
from umnp.proto.message_header import MessageHeader


class Message:
    _registered_types = {}
    _registered_names = {}

    @property
    def message_types(self):
        return self._registered_types

    def __init__(
        self,
        msg_type: int,
        data: bytes,
        sender_id: bytes,
        sender_type: int,
        send_time: typing.Optional[TimeStamp],
    ):
        """
        Parameters
        ----------
        send_time: datetime.datetime, optional
            The timestamp of sending. If None, the current time will be used.
        """
        self._sender_id = sender_id
        self._sender_type = sender_type
        self._data = data
        self._header = MessageHeader(
            msg_type,
            sender_device_type=sender_type,
            sender_device_id=sender_id,
            send_time=send_time,
        )

    @classmethod
    def add_message_type(cls, msg_type: int, msg: typing.Type["Message"], name: str):
        if msg_type in cls._registered_types:
            return
        print(f"Registering message type {msg_type} '{name}'")
        cls._registered_types[msg_type] = msg
        cls._registered_names[msg_type] = name

    @classmethod
    def get_message_type(cls, msg_type: int):
        return cls._registered_types[msg_type]

    def get_message_type_name(self) -> str:
        return self._registered_names[self.type]

    @property
    def type(self) -> int:
        return self._header.message_type

    @property
    def version(self) -> int:
        return self._header.version

    @property
    def sender_type(self) -> int:
        return self._sender_type

    @property
    def sender_id(self) -> str:
        return ":".join(f"{byte:02x}" for byte in self._sender_id)

    @property
    def data(self) -> bytes:
        return self._data

    def encode(self) -> bytes:
        header = MessageHeader(
            msg_type=self.type,
            sender_device_type=self._sender_type,
            sender_device_id=self._sender_id,
        )
        payload = self.data
        payload_length = len(payload).to_bytes(MSG_LEN_PAYLOAD_SIZE, MSG_BYTE_ORDER)
        return header.encode() + payload_length + payload

    @classmethod
    def from_bytes(cls, data: bytes):
        header = MessageHeader.decode(data)
        offset = header.encoded_size_bytes
        try:
            # print("data:", data)
            # print("len(data):", len(data))
            # print("offset:", offset)

            payload_len_bytes = data[offset : offset + MSG_LEN_PAYLOAD_SIZE]
            payload_length = int.from_bytes(payload_len_bytes, MSG_BYTE_ORDER)
            # print(f"{payload_len_bytes=}")
            # print(f"{payload_length=}")
            payload = data[offset + MSG_LEN_PAYLOAD_SIZE :]
            # print(f"expected: {payload=}")
            # raise OSError
        except (KeyError, ValueError):
            log_error("Invalid message: could not extract payload length")
            return None

        offset += MSG_LEN_PAYLOAD_SIZE
        payload = data[offset:]
        if len(payload) != payload_length:
            log_error(
                f"Invalid message: mismatch between specified {payload_length} and actual payload length {len(payload)}."
            )
            return None

        msg = Message.get_message_type(header.message_type).decode(
            payload, header=header
        )
        return msg

    def __str__(self):
        return f"Message of type {self.type}"

    def payload(self) -> str:
        return self._data.decode("utf-8")
