try:
    # noinspection PyUnresolvedReferences
    import typing
except ImportError:
    pass

MSG_STRING_ENCODING: str = "utf-8"
MSG_PROTOCOL_VERSION: int = 0

MSG_BYTE_ORDER: typing.Literal["little", "big"] = "big"

# number of bytes reserved for fields in the message header

# protocol version
MSG_LEN_PROTOCOL_VERSION: int = 2

# message type, see unmp.protocol.messagetype
MSG_LEN_MESSAGE_TYPE: int = 2

# message timestamp (of sending), seconds since epoch for version 0
MSG_LEN_TIMESTAMP: int = 4

# sender unique id (e.g. serial number or mac address)
MSG_LEN_SENDER_ID: int = 6

# sender type (e.g. model of measurement or control device attached to microcontroller)
MSG_LEN_SENDER_TYPE: int = 2

# length of payload
MSG_LEN_PAYLOAD_SIZE: int = 2
