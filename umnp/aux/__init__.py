import datetime


def utc_now_rounded_second() -> datetime.datetime:
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    if now.microsecond >= 500 * 1000:
        now = now + datetime.timedelta(seconds=1)
    now = now.replace(microsecond=0)
    return now
