import logging
import os

from umnp.aux import utc_now_rounded_second


def setup_logger(name: str, log_dir: str | None = None):
    now = utc_now_rounded_second()
    now_string = now.isoformat().replace(":", "-")
    now_string = now_string.split("+")[0]

    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    if log_dir:
        if not os.path.exists(log_dir):
            os.mkdir(log_dir)
        log_file_fn = os.path.join(log_dir, f"{name}-{now_string}.log")
        log_file = logging.FileHandler(log_file_fn)
        log_file.setLevel(logging.DEBUG)
        logger.addHandler(log_file)

    console_logger = logging.StreamHandler()
    logger.addHandler(console_logger)

    return logger
