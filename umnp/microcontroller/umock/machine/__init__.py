import datetime
import uuid

from umnp.proto.message_header import MSG_BYTE_ORDER


def unique_id() -> bytes:
    return uuid.getnode().to_bytes(6, MSG_BYTE_ORDER)


class RTC:
    def __init__(self):
        self._time_offset = 0

    def datetime(self, set_time=None):
        if set_time:
            # save time offset to given time
            self._time_offset = 0

            print("RTC.datetime() setting not implemented")
            return

        now = datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(
            seconds=self._time_offset
        )
        return (
            now.year,
            now.month,
            now.day,
            now.hour,
            now.minute,
            now.second,
            now.microsecond / 1000 / 1000,
        )


class SPI:
    def __init__(self, *args, **kwargs):
        pass

    def deinit(self):
        pass

    def write(self, *args):
        pass


class Pin:
    OUT = 1

    def __init__(self, pin: int, mode: int = -1):
        self._pin = pin
        self._mode = mode

    def __call__(self, value):
        pass


class I2C:
    def __init__(self, id: int, scl: Pin, sda: Pin, timeout=500000):
        self._id = id
        self._scl = scl
        self._sda = sda

    def scan(self) -> list[int]:
        return []

    def writeto(self, addr: int, buf: bytearray, sopt=True):
        return 1

    def readfrom_info(
        self,
        addr: int,
        buf,
        stop=True,
    ):
        return None

    def readfrom(self, addr: int, nbytes, stop=True) -> bytes:
        return b""

    def writevto(self, add: int, vector, stop=True):
        return 1

    def readfrom_mem(self, i2c_address, address, n_bytes):
        return 1

    def writeto_mem(self, i2c_address, address, buffer):
        pass


class UART:
    def __init__(self, id: int): ...

    def init(
        self,
        baudrate=9600,
        bits=8,
        parity=None,
        stop=1,
        tx=None,
        rx=None,
        rts=None,
        cts=None,
        txbuf=None,
        rxbuf=None,
        timeout=None,
        timeout_chars=None,
        invert=None,
        flow=None,
    ): ...

    def any(self) -> int: ...

    def deinit(self): ...

    def read(self, nbytes=None): ...

    def readinto(self, buf, nbytes=None): ...

    def readline(self): ...
    def write(self, buf: bytes): ...

    def sendbread(self): ...

    def flush(self): ...

    def txdone(self): ...
