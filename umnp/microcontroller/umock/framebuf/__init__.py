RGB565 = 1


class FrameBuffer:
    def __init__(self, buffer, width, height, fmt, stride=None):
        self._buffer = buffer
        self._width = width
        self._height = height
        self._format = fmt
        self._stride = stride
        if self._stride is None:
            self._stride = width  # or height? FIXME
        self._fill = None

    def fill(self, *args):
        self._fill = args
