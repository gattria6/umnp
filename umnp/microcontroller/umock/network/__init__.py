from umnp.microcontroller.umock.machine import SPI, Pin


class WIZNET5K:
    def __init__(self, spi: SPI, pin1: Pin, pin2: Pin):
        self._active: bool = False
        self._mac = b"\xe6ad\x08Cx"  # FIXME
        self._ip = "127.0.0.1"
        self._gateway = "127.0.0.2"
        self._netmask = "255.255.255.0"
        self._nameserver = "8.8.8.8"
        self._spi = spi
        self._pin1 = pin1
        self._pin2 = pin2
        self._mac = None

    def active(self, active: bool) -> None:
        self._active = active

    def config(self, *args, **kwargs):
        if len(args) == 1 and args[0] == "mac":
            return self._mac
        if "mac" in kwargs:
            self._mac = kwargs["mac"]

    def ifconfig(self, *args, **kwargs):
        if len(args) == 1 and args[0] == "dhcp":
            return
        if args is None and kwargs is None:
            return self._ip, self._netmask, self._gateway, self._nameserver
