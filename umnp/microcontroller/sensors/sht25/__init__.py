try:
    from machine import I2C
except ImportError:
    from umnp.microcontroller.umock.machine import I2C

import asyncio
import time

# SHT25_READ_T_HOLD =
# SHT25_READ_RH_HOLD =
# SHT25_WRITE_USER_REGISTER =
# SHT25_READ_USER_REGISTER =

SHT25_READ_T_NO_HOLD = 0xF3
SHT25_READ_RH_NO_HOLD = 0xF5
SHT25_SOFT_RESET = 0xFE
NO_HOLD_WAIT_TIME = 18.5 / 1000 / 1000
SHT25_MEASUREMENT_TYPE_T = 0
SHT25_MEASUREMENT_TYPE_RH = 1


class SHT25:
    def __init__(self, i2c: I2C):
        self._i2c = i2c
        self._address = 64
        self._initialised = False
        self._wait_rh_ms = 100
        self._wait_t_ms = 100
        self._nan = float("NAN")
        devices = self._i2c.scan()
        if self._address not in devices:
            print("Device not found")
            return
        if self.reset():
            print("Initialised")
            self._initialised = True
        else:
            print("Could not initialise device")

    @property
    def initialised(self) -> bool:
        return self._initialised

    def _command(self, command: int) -> int:
        pass

    @staticmethod
    def _crc8(data):
        # CRC-8-Dallas/Maxim for I2C with 0x31 polynomial
        crc = 0x0
        for byte in data:
            crc ^= byte
            for _ in range(8):
                if crc & 0x80:
                    crc = (crc << 1) ^ 0x31
                else:
                    crc = crc << 1
                crc &= 0xFF

        return crc

    def reset(self) -> bool:
        cmd = bytearray(1)
        cmd[0] = SHT25_SOFT_RESET
        n_ack = self._i2c.writeto(self._address, cmd)
        if n_ack == 1:
            return True
        return False

    def _decode(self, buffer):
        lsb = buffer[1]
        msb = buffer[0]
        measurement_type = (lsb & 0x2) >> 1
        lsb = lsb & ~0x03

        if self._crc8(buffer[:2]) != buffer[2]:
            return self._nan, self._nan

        return lsb + (msb << 8), measurement_type

    def _translate_temperature(self, buffer) -> float:
        t_raw, measurement_type = self._decode(buffer)
        if measurement_type != SHT25_MEASUREMENT_TYPE_T:
            return self._nan

        return -46.85 + 175.72 * t_raw / 2**16

    def _translate_rh(self, buffer):
        rh_raw, measurement_type = self._decode(buffer)
        if measurement_type != SHT25_MEASUREMENT_TYPE_RH:
            return self._nan

        return -6 + 125 * rh_raw / 2**16

    async def measure(self):
        temperature = self._nan
        rh = self._nan
        if not self.initialised:
            # print("Not initialised")
            return temperature, rh

        cmd = bytearray(1)
        cmd[0] = SHT25_READ_T_NO_HOLD
        n_ack = self._i2c.writeto(self._address, cmd)
        # print("T req n_ack" ,n_ack)
        if n_ack != 1:
            return temperature, rh

        time.sleep(NO_HOLD_WAIT_TIME * 2)
        await asyncio.sleep(self._wait_t_ms / 1000)
        data = self._i2c.readfrom(self._address, 3)
        temperature = self._translate_temperature(data)
        data = None

        cmd[0] = SHT25_READ_RH_NO_HOLD
        n_ack = self._i2c.writeto(self._address, cmd, False)
        time.sleep(NO_HOLD_WAIT_TIME * 2)
        self._i2c.readfrom(self._address, 0)  # send stop signal
        await asyncio.sleep(self._wait_rh_ms / 1000)

        data = self._i2c.readfrom(self._address, 3)
        rh = self._translate_rh(data)

        return round(temperature, 3), round(rh, 3)
