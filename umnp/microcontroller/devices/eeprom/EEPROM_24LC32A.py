import sys

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    from machine import I2C
else:
    from umnp.microcontroller.umock.machine import I2C

I2C_ADDRESS_EEPROM24LC32A = 0x50


class EEPROM24LC32A:
    def __init__(self, i2c: I2C, i2c_address=I2C_ADDRESS_EEPROM24LC32A):
        self.__block_size = 32
        self.__max_size = 4096
        self.__block_count = self.__max_size // self.__block_size

        self.__i2c_address = i2c_address
        self.__i2c = i2c
        self.count = self.__block_count

        if self.__i2c_address not in self.__i2c.scan():
            raise RuntimeError(f"No I2C device at address {self.__i2c_address} found")

    def __calculate_address(self, block_idx, offset=0):
        address = block_idx * self.__block_size + offset
        if address > self.__max_size or address < 0:
            raise RuntimeError(
                f"Invalid address {address}, out of range [0, {self.__max_size}]"
            )
        result = bytearray(2)
        result[0] = address >> 8
        result[1] = address & 0xFF

    def __set_read_address(self, address):
        self.__i2c.writeto(self.__i2c_address, address)

    def readblocks(self, block_num, buf, offset=0):
        """
        Parameters
        ----------
        block_num
        buf
        offset

        Returns
        -------

        According to https: // docs.micropython.org / en / latest / library / os.html  # os.AbstractBlockDev
        if offset = 0:
           read multiple aligned blocks (count given by the length of buf, which is a multiple of the block size)
        else:
           read arbitrary number of bytes from an arbitrary location

        We do not differentiate between those two cases, as the EEPROM only supports random reads (either single byte
        or sequential)
        """
        bytes_to_read = len(buf)
        if offset == 0 and bytes_to_read % self.__block_size != 0:
            raise RuntimeError("Buffer length not a multiple of the block size")
        if bytes_to_read + block_num * self.__block_size + offset > self.__max_size:
            raise RuntimeError("Error: operation would read beyond the maximum address")
        address = self.__calculate_address(block_num, offset)
        self.__set_read_address(address)

        # we don't want to overwrite buf, but write into it
        buf[0:bytes_to_read] = self.__i2c.readfrom(self.__i2c_address, bytes_to_read)

    def __write_page(self, address, data):
        self.__i2c.writevto(self.__i2c_address, (address, data))

    def writeblocks(self, block_num: int, buf: bytearray, offset=0) -> None:
        """
        Parameters
        ----------
        block_num: int
        buf: bytearray
        offset: int

        Returns
        -------

        According to https://docs.micropython.org/en/latest/library/os.html#os.AbstractBlockDev
        if offset = 0:
           read multiple aligned blocks (count given by the length of buf, which is a multiple of the block size)
        else:
           read arbitrary number of bytes from an arbitrary location

        We need to differentiate, because the EEPROM supports two modes:
        * single byte write: writes at random addresses
        * page writes: write 32 bytes at once at a random address, but wrap around at the page boundary,
          potentially overwriting data - so we need to ensure that we start a page write at a page boundary so we
          don't cross page boundaries
        """

        bytes_to_write = len(buf)

        if bytes_to_write + block_num * self.__block_size + offset > self.__max_size:
            raise RuntimeError("Error: operation would read beyond the maximum address")

        if offset == 0:
            block_count, remainder = divmod(bytes_to_write, self.__block_size)
            if remainder != 0:
                raise RuntimeError("Buffer length not a multiple of the block size")
            for i in range(block_count):
                address = self.__calculate_address(block_num + i, 0)
                start = i * self.__block_size
                end = start + self.__block_size
                self.__write_page(address, buf[start:end])

        else:
            block_count, remainder = divmod(bytes_to_write, self.__block_size)
            if remainder != self.__block_size - offset:
                raise RuntimeError(
                    "Buffer length not a multiple of the block size + offset"
                )

            address = self.__calculate_address(block_num, offset)
            self.__write_page(address, buf[: self.__block_size - offset])

            for i in range(block_count):
                address = self.__calculate_address(block_num + i + 1, 0)
                start = i * self.__block_size + self.__block_size - offset
                end = start + self.__block_size
                self.__write_page(address, buf[start:end])


def ioctl(self, op, arg):
    if op == 4:  # block count
        return self.__block_count
    if op == 5:  # block size
        return self.__block_size

    # Needed for littlefs
    if op == 6:  # erase
        return 0
