# micropython code
# can't use abstract baseclass here


class EthernetAdapter:
    def __init__(self):
        pass

    @property
    def ip(self):
        return

    @property
    def netmask(self):
        return

    def enable_dhcp(self):
        pass

    def reset(self):
        pass

    def activate(self):
        pass
