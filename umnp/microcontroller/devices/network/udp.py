import select
import socket
import sys

from umnp.microcontroller.devices.network import (
    LISTEN_TIMEOUT_MS,
    calculate_broadcast_ip,
)
from umnp.proto.common.logging import log_error

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import uasyncio as asyncio
else:

    import asyncio
DEFAULT_UMNP_DATA_IN_PORT = 7777
DEFAULT_UMNP_COMMAND_IN_PORT = 7776


class UDPReceiver:
    def __init__(
        self, listen_ip: str, listen_port: int, timeout: int = LISTEN_TIMEOUT_MS
    ):
        self.socket = None
        self._listen_port = listen_port
        self.listen_ip = listen_ip
        self.timeout = timeout
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.setblocking(False)
        self.socket.bind((listen_ip, listen_port))
        self.poller = select.poll()
        self.poller.register(self.socket, select.POLLIN)

    async def receive(self, controller):
        timeout = self.timeout
        while True:
            if self.poller.poll(timeout):
                buffer, address = self.socket.recvfrom(1024)
                await controller.queue_incoming_message(buffer, address)
            await asyncio.sleep(0)


class UDPSender:
    def __init__(self, ip, netmask, send_to_port: int):
        self.socket = None
        self.ip = ip
        self.netmask = netmask
        self._target_port = send_to_port
        self._error = False

        if ip and netmask:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.broadcast_ip = calculate_broadcast_ip(ip, netmask)

    @property
    def error(self):
        return self._error

    def reset_error(self):
        self._error = False

    async def broadcast(self, msg):
        # print("sending %s" % msg)
        if isinstance(msg, str):
            msg = msg.encode("utf-8")
        if self.socket:
            try:
                self.socket.sendto(msg, (self.broadcast_ip, self._target_port))
            except OSError as e:
                self._error = True
                log_error(str(e))
