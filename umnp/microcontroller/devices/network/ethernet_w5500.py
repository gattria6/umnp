import sys
import time

from umnp.microcontroller.devices.network.ethernet import EthernetAdapter

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import machine

    # noinspection PyUnresolvedReferences
    import network


else:
    from umnp.microcontroller.umock import machine, network

from umnp.proto.common.logging import log_info


class EthernetW5500(EthernetAdapter):
    def __init__(
        self,
        spi: machine.SPI,
        pin_cs: machine.Pin,
        pin_rst: machine.Pin,
        mac: bytes,
        dhcp=False,
    ):
        super().__init__()
        self._dhcp = dhcp
        self._spi = spi
        self._mac = mac
        self._pin1 = pin_cs
        self._pin2 = pin_rst
        self._nic = None
        self._ip = None
        self._subnet = None
        self._gateway = None
        self._dns = None
        self._init(spi)

        # self._nic = network.WIZNET5K(spi, self._pin1, self._pin2)
        # self._nic.active(True)

        # self._nic.config(mac=self._mac)
        # if self._dhcp:
        #    self.enable_dhcp()

    def _init(self, spi: machine.SPI):
        self._nic = network.WIZNET5K(spi, self._pin1, self._pin2)
        self._nic.active(True)
        self._nic.config(mac=self._mac)
        if self._dhcp:
            self.enable_dhcp()

    def reinit(self, spi: machine.SPI):
        self._init(spi)
        self.set_network(
            ip=self._ip, subnet_mask=self._subnet, gateway=self._gateway, dns=self._dns
        )

    def set_network(self, ip: str, subnet_mask: str, gateway: str, dns: str):
        self._ip = ip
        self._subnet = subnet_mask
        self._gateway = gateway
        self._dns = dns
        self._nic.ifconfig((ip, subnet_mask, gateway, dns))

    def enable_dhcp(self):
        while True:
            print("Requesting IP via DHCP")
            try:
                self._nic.ifconfig("dhcp")
                return
            except OSError:
                pass
            print(self._nic.ifconfig())

    @property
    def netmask(self):
        return self._nic.ifconfig()[1]

    def activate(self):
        self._nic.active(True)

    def deactivate(self):
        self._nic.active(False)

    @property
    def ip(self):
        return self._nic.ifconfig()[0]

    @property
    def mac(self):
        return self._nic.config("mac")

    def reset(self):
        log_info("Resetting NIC")
        self._nic.active(False)
        time.sleep(0.1)
        self._nic.active(True)
