LISTEN_TIMEOUT_MS = 1


def calculate_broadcast_ip(ip: str, mask: str) -> str:
    if ip is None or mask is None:
        return ""

    ip = [int(x) for x in ip.split(".")]
    mask = [int(x) for x in mask.split(".")]

    if len(ip) != 4 or len(mask) != 4:
        return ""

    bc = [(i | ~j) & 0xff for i, j in zip(ip, mask)]
    bc = ".".join(str(x) for x in bc)
    return bc
