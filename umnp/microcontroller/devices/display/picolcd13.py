import sys

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import framebuf
    # noinspection PyUnresolvedReferences
    import machine
    # noinspection PyUnresolvedReferences
    from micropython import const
else:
    from umnp.microcontroller.umock import machine, network, framebuf
    from umnp.microcontroller.umock.micropython import const

_BL = const(13)
_DC = const(8)
_RST = const(12)
_MOSI = const(11)
_SCK = const(10)
_CS = const(9)
_WIDTH = const(240)
_HEIGHT = const(240)


def colour(red, green, blue):  # Convert RGB888 to RGB565    # copied from? FIXME
    return (
            (((green & 0b00011100) << 3) + ((blue & 0b11111000) >> 3) << 8) +
            (red & 0b11111000) + ((green & 0b11100000) >> 5)
    )


class PicoLCD13(framebuf.FrameBuffer):
    def __init__(self):
        self.width = _WIDTH
        self.height = _HEIGHT
        self.cs = machine.Pin(_CS, machine.Pin.OUT)
        self.rst = machine.Pin(_RST, machine.Pin.OUT)

        self.cs(1)
        self.spi = machine.SPI(1, 100_000_000, polarity=0, phase=0,
                               sck=machine.Pin(_SCK),
                               mosi=machine.Pin(_MOSI),
                               miso=None)
        self.dc = machine.Pin(_DC, machine.Pin.OUT)
        self.dc(1)
        self.buffer = bytearray(self.height * self.width * 2)
        super().__init__(self.buffer, self.width, self.height, framebuf.RGB565)

        self.red = 0x07E0
        self.green = 0x001f
        self.blue = 0xf800
        self.white = 0xffff
        self._bg_color = 0

        self._initialise_display()

        self.clear()
        self.update()

    @property
    def bg_color(self):
        return self._bg_color

    def _write_cmd(self, cmd):
        self.cs(1)
        self.dc(0)
        self.cs(0)
        self.spi.write(bytearray([cmd]))
        self.cs(1)

    def _write_data(self, buf):
        self.cs(1)
        self.dc(1)
        self.cs(0)
        self.spi.write(bytearray([buf]))
        self.cs(1)

    def _config(self, cmd, data):
        self._write_cmd(cmd)
        for d in data:
            self._write_data(d)

    def clear(self, r=0, g=0, b=0):
        self.fill(colour(r, g, b))

    def _initialise_display(self):
        self.rst(1)
        self.rst(0)
        self.rst(1)

        self._config(0x36, (0x70,))  # Address Order setup
        self._config(0x3a, (0x05,))  # Interface Pixel Format
        self._config(0xB2, (0x0c, 0x0c, 0x00, 0x33, 0x33))  # Porch setting (back, front, 0, b partial, f part)
        self._config(0xb7, (0x35,))  # gate control
        self._config(0xbb, (0x19,))  # VCOMS setting
        self._config(0xc0, (0x2c,))  # LCM Control
        self._config(0xc2, (0x01,))  # VDV and VRH Command Enable
        self._config(0xc3, (0x12,))  # VRH Set
        self._config(0xc4, (0x20,))  # VRV Set
        self._config(0xc6, (0x0f,))  # Frame Rate Control in normal mode
        self._config(0xd0, (0xa4, 0xa1,))  # Power Control 1
        self._config(0xe0,  # Positive voltage gamma control
                     (0xD0, 0x04, 0x0D, 0x11, 0x13, 0x2B, 0x3F, 0x54, 0x4C, 0x18, 0x0D, 0x0B, 0x1F, 0x23))

        self._config(0xe1,  # Negative voltage gamma control
                     (0xD0, 0x04, 0x0C, 0x11, 0x13, 0x2C, 0x3F, 0x44, 0x51, 0x2F, 0x1F, 0x1F, 0x20, 0x23))

        self._write_cmd(0x21)  # Display inversion on
        self._write_cmd(0x11)  # Turn off sleep mode
        self._write_cmd(0x29)  # Display on

    def update(self):
        self._config(0x2a, (0x00, 0x00, 0x00, 0xef))  # column address set
        self._config(0x2b, (0x00, 0x00, 0x00, 0xef))  # row address set
        self._write_cmd(0x2C)  # memory set

        self.cs(1)
        self.dc(1)
        self.cs(0)
        self.spi.write(self.buffer)
        self.cs(1)
