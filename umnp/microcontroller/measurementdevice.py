import binascii
import sys
import time

from umnp.devices import DEVICE_TYPE_UNKNOWN
from umnp.microcontroller.communication.udp_communicator import UDPCommunicator
from umnp.microcontroller.devices.network.udp import (
    UDPSender,
    UDPReceiver,
    DEFAULT_UMNP_DATA_IN_PORT,
    DEFAULT_UMNP_COMMAND_IN_PORT,
)
from umnp.proto import DeviceMessage
from umnp.proto.common.logging import log_error
from umnp.proto.constants import MSG_STRING_ENCODING
from umnp.proto.device_message import MSG_TYPE_LIFE_SIGN, MSG_TYPE_INFO

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import machine
else:
    from umnp.microcontroller.umock import machine


class MeasurementDevice:
    def __init__(self, device_type: int = DEVICE_TYPE_UNKNOWN):
        self._boot_time = time.time()
        self._identifier_raw = machine.unique_id()
        if len(self._identifier_raw) > 6:
            self._identifier_raw = self._identifier_raw[:6]
        elif len(self._identifier_raw) < 6:
            self._identifier_raw = (
                b"0" * (6 - len(self._identifier_raw)) + self._identifier_raw
            )
        self._identifier = binascii.hexlify(self.identifier_raw).decode(
            MSG_STRING_ENCODING
        )
        self._network = None
        self._sender = None
        self._receiver = None
        self._communicator = None
        self._type = device_type
        self._i2c = None
        self._spi = None
        self._spi_params = None

    @property
    def device_type(self):
        return self._type

    @property
    def boot_time(self):
        return self._boot_time

    @property
    def spi(self):
        return self._spi

    def spi_reinit(self) -> machine.SPI:
        if len(self._spi_params) == 0:
            return

        if self._spi:
            print("SPI::reinit()")
            self._spi.deinit()
        self._spi = machine.SPI(
            self._spi_params["id"],
            self._spi_params["baud"],
            mosi=self._spi_params["mosi"],
            miso=self._spi_params["miso"],
            sck=self._spi_params["sck"],
        )
        return self._spi

    def add_spi(
        self,
        spi_id: int,
        baudrate: int,
        mosi: machine.Pin,
        miso: machine.Pin,
        sck: machine.Pin,
    ) -> machine.SPI:

        self._spi_params = {
            "id": spi_id,
            "baud": baudrate,
            "mosi": mosi,
            "miso": miso,
            "sck": sck,
        }

        self._spi = self.spi_reinit()
        return self._spi

    def add_i2c(self, i2c_id: int, scl: machine.Pin, sda: machine.Pin, timeout: int):
        self._i2c = machine.I2C(id=i2c_id, scl=scl, sda=sda, timeout=timeout)
        return self._i2c

    @property
    def i2c(self):
        return self._i2c

    def add_network_adapter(self, adapter):
        self._network = adapter

    async def reset_network(self):
        print("Network error")
        if self.spi:
            self.spi_reinit()
            # dev_mac = device.generated_mac_raw()
            self.network.deactivate()
            self.network.reinit(self.spi)
            self.communicator.clear_network_error()

            info_msg = DeviceMessage(
                "restarted network",
                MSG_TYPE_INFO,
                self.identifier_raw,
                self.device_type,
            )
            await self.communicator.send_message(info_msg)
            info_msg = None

    @property
    def network(self):
        return self._network

    def generated_mac_string(self) -> str:
        machine_id = self.identifier_raw[:6]
        return ":".join(f"{digit:02x}" for digit in machine_id)

    def generated_mac_raw(self) -> bytes:
        return self.identifier_raw[:6]

    @property
    def identifier_raw(self) -> bytes:
        return self._identifier_raw

    @property
    def identifier(self) -> str:
        return self._identifier

    def create_sender(self, port: int = DEFAULT_UMNP_DATA_IN_PORT):
        if not self._network:
            raise ValueError("No network adapter added")
        if not self._sender:
            self._sender = UDPSender(
                self._network.ip, self._network.netmask, send_to_port=port
            )
        return self._sender

    def create_receiver(self, port: int = DEFAULT_UMNP_COMMAND_IN_PORT):
        if not self._network:
            raise ValueError("No network adapter added")
        if not self._receiver:
            self._receiver = UDPReceiver(self._network.ip, listen_port=port)
        return self._receiver

    def create_communicator(
        self,
        data_port: int = DEFAULT_UMNP_DATA_IN_PORT,
        cmd_port: int = DEFAULT_UMNP_COMMAND_IN_PORT,
    ):

        r = None
        s = None
        if not self._communicator:
            try:
                s = self.create_sender(port=data_port)
                r = self.create_receiver(port=cmd_port)
            except ValueError:
                log_error("No network hardware available")
                
            self._communicator = UDPCommunicator(receiver=r, sender=s, device=self)

        return self._communicator

    @property
    def communicator(self) -> UDPCommunicator | None:
        return self._communicator

    async def send_life_sign(self):
        if not self.communicator:
            return
        if self.communicator.network_error:
            return

        now = time.time()
        seconds_since_boot = now - self.boot_time

        msg = DeviceMessage(
            f"{seconds_since_boot}",
            MSG_TYPE_LIFE_SIGN,
            self.identifier_raw,
            self.device_type,
        )

        await self.communicator.send_message(msg)
