import sys

from umnp.communication.abstract_serial_connection import AbstractSerialConnection

if sys.implementation.name == "micropython":
    import machine
else:
    from umnp.microcontroller.umock import machine


class UARTSerial(AbstractSerialConnection):
    def __init__(self, address: machine.UART):
        super().__init__(address)
