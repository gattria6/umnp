import sys
import time

from umnp.microcontroller.devices.network.udp import UDPSender, UDPReceiver
from umnp.microcontroller.tasks.periodictask import PeriodicTask
from umnp.proto.message import Message

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import uasyncio as asyncio

    # noinspection PyUnresolvedReferences
    import machine
else:
    import asyncio
    from typing import TYPE_CHECKING

    if TYPE_CHECKING:
        from umnp.microcontroller.measurementdevice import MeasurementDevice


class UDPCommunicator:
    def __init__(
        self,
        sender: UDPSender | None,
        receiver: UDPReceiver | None,
        device: "MeasurementDevice",
        max_msgs: int = 10,
    ):
        self._receive_lock = asyncio.Lock()
        self._send_lock = asyncio.Lock()

        self._messages_received = []
        self._messages_send_queue = []
        self._max_msgs = max_msgs
        self._sender = sender
        self._receiver = receiver
        self._tasks = {}
        self._device = device

    @property
    def sender(self):
        return self._sender

    @property
    def network_error(self):
        if self._sender:
            return self._sender.error
        else:
            return None

    def clear_network_error(self):
        if self._sender:
            self._sender.reset_error()

    async def queue_incoming_message(self, msg, source):
        if self._receiver is None:
            return

        async with self._receive_lock:
            self._messages_received.append((msg, source, time.time()))
            while len(self._messages_received) > self._max_msgs:
                self._messages_received.pop(0)

    async def get_newest_message(self):
        if self._receiver is None:
            return

        async with self._receive_lock:
            if len(self._messages_received):
                return self._messages_received.pop(0)
            else:
                return None

    async def receive_task(self):
        while True:
            if self._receiver:
                await self._receiver.receive(self)
            await asyncio.sleep(0.500)

    async def send_task(self):
        while True:
            msg = None
            async with self._send_lock:
                if len(self._messages_send_queue) > 0:
                    msg = self._messages_send_queue.pop()

            if msg is None:
                await asyncio.sleep(0.5)
                continue

            if self._sender is None:
                await asyncio.sleep(0.1)
                continue

            if isinstance(msg, Message):
                await self._sender.broadcast(msg.encode())

            await asyncio.sleep(0.5)

    async def send_message(self, msg: Message):
        if self._sender is None:
            return

        self._messages_send_queue.append(msg)

    async def control_task(self):
        while True:
            async with self._receive_lock:
                msg = await self.get_newest_message()
                if msg is not None:
                    pass
            await asyncio.sleep(0.5)

    async def start(self):
        receive_task = asyncio.create_task(self.receive_task())
        send_task = asyncio.create_task(self.send_task())
        control_task = asyncio.create_task(self.control_task())

        tasks = []

        for name, task in self._tasks.items():
            result = asyncio.create_task(task.run())
            tasks.append(result)

        for t in tasks:
            await t

        await receive_task
        await send_task
        await control_task

    def add_task(self, task: PeriodicTask, name: str):
        self._tasks[name] = task
