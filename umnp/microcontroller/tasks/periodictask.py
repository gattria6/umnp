import sys
import time

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import uasyncio as asyncio
else:
    import asyncio


class PeriodicTask:
    def __init__(
        self, function: callable, async_call_back: callable, every_ms: int, *args
    ):
        self._function = function
        self._args = args
        self._every_ms = every_ms
        self._call_back = async_call_back

    async def run(self):
        start_time = time.ticks_ms()
        last = start_time
        func = self._function
        args = self._args
        call_back = self._call_back
        delay_seconds = 25 / 1000

        while True:
            last = time.ticks_ms()
            result = await func(*args)
            if call_back:
                await call_back(result)

            while True:
                now = time.ticks_ms()
                diff = time.ticks_diff(now, last)
                # print(last, now, diff)
                if diff > self._every_ms:
                    last = now
                    break
                await asyncio.sleep(delay_seconds)
