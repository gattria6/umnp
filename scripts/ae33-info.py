import time

import serial

conn = serial.Serial(
    port="/dev/ttyUSB0",
    baudrate=115200,
    dsrdtr=True,
    xonxoff=False,
    rtscts=False,
    timeout=0.5,
)


print("Connected")
time.sleep(1)
print()
print("get newest log")
conn.write(b"$AE33:L1\r")
print(conn.readline())

time.sleep(1)
print()
print("get tape advances")
conn.write(b"$AE33:A\r")
print(conn.readlines())

time.sleep(1)
print()
print("get setup file")
conn.write(b"$AE33:SG\r")
print(conn.readlines())
print()

time.sleep(1)
print("start measurement")
conn.write(b"$AE33:X1\r")
print(conn.readlines())
print()
