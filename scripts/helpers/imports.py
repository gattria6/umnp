import ast
import os


def get_module_names(node: ast.Import):
    names = [n.name for n in node.names]
    return names


def import_names(node):
    result = []
    if isinstance(node, ast.Import):
        print("\t", ",".join(get_module_names(node)))
        result.extend(get_module_names(node))
    elif isinstance(node, ast.ImportFrom):
        print("\t", node.module)
        result.append(node.module)

    if isinstance(node, ast.Try):
        for child in ast.iter_child_nodes(node):
            result.extend(import_names(child))
        for handler in node.handlers:
            for child in ast.iter_child_nodes(handler):
                result.extend(import_names(child))

    return result


def get_imported_files(
    fn,
    modules=None,
    files=None,
    filter_by: str | None = None,
    ignore: str | None = None,
):
    if modules is None:
        modules = []
    if files is None:
        files = []

    current_modules = []

    with open(fn) as f:
        print(f"Reading {fn}")
        root = ast.parse(f.read(), fn)
        for node in ast.iter_child_nodes(root):
            current_modules.extend(import_names(node))

    current_modules = sorted(list(set(current_modules)))
    for module in current_modules:
        if module in modules:
            pass

        module_fn = module.replace(".", os.sep)

        if (filter_by and module_fn.startswith(filter_by)) or filter_by is None:
            if ignore is not None and ignore in module_fn:
                continue

            if os.path.exists(module_fn) and os.path.isdir(module_fn):
                module_fn = os.path.join(module_fn, "__init__.py")
            else:
                module_fn = module_fn + ".py"
            modules.append(module)
            files.append(module_fn)
            files, modules = get_imported_files(
                module_fn, modules, files, filter_by=filter_by, ignore=ignore
            )

    files = list(sorted(list(set(files))))
    modules = list(sorted(list(set(modules))))
    return files, modules
