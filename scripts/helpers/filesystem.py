import os


def get_directory_tree_from_directories(directories: list[str]) -> list[str]:
    dirs = list(set([os.path.split(d)[0] for d in directories]))
    all_dirs = dirs
    for d in dirs:
        while d:
            d = os.path.split(d)[0]
            if d in all_dirs:
                continue
            all_dirs.append(d)
    return list(sorted(list(set(dirs))))
