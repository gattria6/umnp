import subprocess


def parse_git_status(state: str) -> str:
    if len(state) == 0 or len(state) > 2:
        raise ValueError
    if "M" in state:
        return "modified"
    if "??" in state:
        return "untracked"
    if "D" in state:
        return "deleted"
    if "A" in state:
        return "added"
    if "T" in state:
        return "type changed"
    if "R" in state:
        return "renamed"
    if "C" in state:
        return "copied"


def get_git_unclean_files() -> dict[str:str]:
    """
    Returns a dictionary of unclean files of the git repository, in which the current directory resides.

    Raises
    ------
    ValueError
        If 'git status' can't be executed or fails with an error

    Returns
    -------
    dict[str: str]
        Keys of this dictionary are the paths of 'dirty' files (e.g. uncommitted and untracked files),
        and a description as value.
    """

    git_status = {}
    cmd = ["git", "status", "--porcelain=v1"]
    try:
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        proc.wait()
    except FileNotFoundError:
        raise ValueError("git executable not found")

    if proc.returncode != 0:
        error = "\n".join([x.decode("utf-8") for x in proc.stderr])
        raise ValueError(f"Error running {' '.join(cmd)}: {error}")

    for line in proc.stdout:
        fields = line.decode("utf-8").split()
        status = fields[0]
        file = " ".join(fields[1:])
        if file.startswith('"') and file.endswith('"'):
            file = file[1:-1]
        git_status[file] = parse_git_status(status)
    return git_status
