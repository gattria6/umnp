import datetime
import os
import subprocess


def upload_file(port: str, local_path: str, remote_path: str | None = None):
    if remote_path is None:
        remote_path = local_path
    if not os.path.exists(local_path):
        raise ValueError("fFile {fn} does not exist")
    if remote_path != local_path:
        print(f"Uploading file {local_path} as {remote_path}")
    else:
        print(f"Uploading file {local_path}")
    cmd = ["ampy", "--port", port, "put", local_path, remote_path]
    subprocess.run(cmd)


def list_remote_file_dates_and_directories(port):
    cmd = ["ampy", "--port", port, "run", "umnp/tools/list-files.py"]
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    files = {}
    dirs = []
    for line in proc.stdout:
        line = line.decode("utf-8")
        fn, size, _, f_type, date = line.split()
        while fn.startswith("/"):
            fn = fn[1:]
        if f_type == "f":
            files[fn] = datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")
        elif f_type == "d":
            dirs.append(fn)

    return files, dirs


def make_remote_directory(path: str, port: str):
    if not path:
        return
    print(f"Creating directory {path}")
    cmd = ["ampy", "--port", port, "mkdir", path]
    subprocess.run(cmd)
