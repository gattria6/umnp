import logging
import time

from umnp.aux.log_helper import setup_logger
from umnp.communication.serial_connection import SerialConnection
from umnp.devices.aethalometer.ae33 import AE33

logger = setup_logger("ae33-test", "logs")


def main():
    conn_opts = {"baudrate": 115200, "dsrdtr": True, "rtscts": False, "xonoff": False}
    port = "/dev/ttyUSB2"
    connection = SerialConnection(port, options=conn_opts)

    ae33 = AE33(connection)
    while True:
        current = ae33.current_measurement()
        if current:
            logging.info(current)
        time.sleep(1)


if __name__ == "__main__":
    main()
