#! /usr/bin/env/python3

"""
upload-script.py

Uploads a Python script, including imported dependencies to a microcontroller using ampy.
Unless otherwise specified, the upload will only occur, if the files do not contain any changes not yet committed
to git, i.e. the working tree (or the parts of it that will be uploaded) are not dirty.
If a file to be uploaded already exists on the microcontroller, it will only be uploaded if newer.

Example:
python ./upload-script.py programs/sensor_calibration.py  --allow-dirty --port "/dev/ttyACM0"

"""
import argparse
import datetime
import os
import sys

# this is ugly and probably not recommended, but enables imports relative to this file
sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from helpers.ampy import (
    upload_file,
    list_remote_file_dates_and_directories,
    make_remote_directory,
)

from helpers.filesystem import get_directory_tree_from_directories
from helpers.git import get_git_unclean_files
from helpers.imports import get_imported_files


def remote_file_older_or_missing(path: str, remote_file_date: datetime.datetime | None):
    if not remote_file_date:
        return True

    disk_date = datetime.datetime.fromtimestamp(os.path.getmtime(path))
    if disk_date > remote_file_date:
        return True

    return False


def make_directories(port: str, local_dirs: list[str], remote_dirs: list[str]):
    for path in local_dirs:
        if path in remote_dirs:
            print(f"Skipping creation of '{path}': directory already exists")
            continue
        make_remote_directory(path, port=port)


def upload_files(local_files, script, port: str, ignore_file_times=False):
    remote_file_dates, remote_directories = list_remote_file_dates_and_directories(port)
    local_directories = get_directory_tree_from_directories(local_files)
    make_directories(port, local_directories, remote_directories)

    for file_path in local_files:
        remote_date = remote_file_dates.get(file_path)
        local_newer = remote_file_older_or_missing(file_path, remote_date)
        if ignore_file_times or local_newer:
            upload_file(port=port, local_path=file_path)
        else:
            print(f"Skipping '{file_path}': remote file is newer")

    upload_file(port, script, "main.py")


def check_for_unclean_files(files_to_upload: list[str], args):
    if not args.no_git:
        return
    errors = 0
    unclean_files = get_git_unclean_files()

    for fn in files_to_upload:
        if fn in unclean_files:
            state = unclean_files[fn]
            print(f"Warning: file '{fn}': {state}")
            errors += 1

    if errors == 0:
        return

    print("Warning: files to upload contain uncommitted changes.")
    if args.allow_dirty:
        print("Continuing with dirty working tree")
        return

    print("Please either commit and retry, or re-run this script with --dirty")
    exit(1)


def main(args):
    fn = args.script
    files, modules = get_imported_files(fn, filter_by="umnp", ignore="mock")
    check_for_unclean_files(files, args)
    upload_files(files, fn, port=args.port, ignore_file_times=args.ignore_time)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog=sys.argv[0])
    parser.add_argument(
        "--allow-dirty",
        action="store_true",
        help="allow upload even when changes to uploaded files are not yet committed to git",
    )
    parser.add_argument(
        "--ignore-time",
        action="store_true",
        help="ignore file timestamps and always upload, even when the "
        "files on the disk are older than on the device",
    )
    parser.add_argument(
        "--no-git", action="store_true", help="don't check for git status"
    )
    parser.add_argument(
        "--port", help="address or port of the attached micro-controller", required=True
    )

    parser.add_argument(
        "script",
        help="Name of the python script file to upload. Will be renamed to main.py",
    )

    args_ = parser.parse_args()
    if not os.path.exists(args_.script):
        print(f"File '{args_.script}' does not exist")
        exit(1)
    if not os.path.exists(args_.port):
        print(f"Can't connect to '{args_.port}': no such file")
        exit(1)

    main(args_)
