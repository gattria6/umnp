import time

import serial

conn = serial.Serial(
    port="/dev/ttyUSB0",
    baudrate=115200,
    dsrdtr=True,
    xonxoff=False,
    rtscts=False,
    timeout=0.5,
)


print("Connected")


for i in range(5):
    conn.write(b"$AE33:D1\r")
    print(conn.readline())
    time.sleep(0.2)
