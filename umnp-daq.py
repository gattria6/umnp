import datetime
import socket

from umnp.daq.file_daq import FileDAQ
from umnp.microcontroller.devices.network.udp import DEFAULT_UMNP_DATA_IN_PORT
from umnp.proto import DataMessage, DeviceMessage
from umnp.proto.message import Message

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(("0.0.0.0", DEFAULT_UMNP_DATA_IN_PORT))


def main():
    files = FileDAQ()
    # print("receive-time,sender-id,T,rH,p,T(p)")
    while True:
        data, addr = sock.recvfrom(2048)

        msg = Message.from_bytes(data)
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        if isinstance(msg, DataMessage):
            print(f"{now},{msg.sender_id},{msg.payload()}")
        elif isinstance(msg, DeviceMessage):
            print(
                f"{now},{msg.sender_id},{msg.get_message_type_string},{msg.payload()}"
            )
        files.save_message(msg)


if __name__ == "__main__":
    main()
