import sys

from umnp.microcontroller.communication.udp_communicator import UDPCommunicator
from umnp.microcontroller.devices.network.ethernet_w5500 import EthernetW5500
from umnp.microcontroller.devices.network.udp import UDPSender, UDPReceiver
from umnp.microcontroller.measurementdevice import MeasurementDevice
from umnp.microcontroller.sensors.sht25 import SHT25
from umnp.microcontroller.tasks.periodictask import PeriodicTask

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import machine

    # noinspection PyUnresolvedReferences
    import uasyncio as asyncio
else:
    from umnp.microcontroller.umock import machine
    import asyncio


def test_function(*args):
    print("test_function called with: ", *args)
    result = " - ".join(*args)
    print(f"test_function returns '{result}'")
    return result


def main():
    # configure network
    device = MeasurementDevice()
    spi = machine.SPI(
        0, 2_000_000, mosi=machine.Pin(19), miso=machine.Pin(16), sck=machine.Pin(18)
    )
    ether = EthernetW5500(spi, 17, 20, mac=device.generated_mac_raw(), dhcp=True)
    device.add_network_adapter(ether)

    i2c = machine.I2C(id=1, scl=machine.Pin(27), sda=machine.Pin(26))
    sht25 = SHT25(i2c)

    sender = UDPSender(ether.ip, ether.netmask, 7777)
    receiver = UDPReceiver(ether.ip, 7776)
    comm = UDPCommunicator(
        receiver=receiver, sender=sender, device_id=device.identifier
    )

    x = PeriodicTask(test_function, print, 1000)
    comm.add_task(x, "test_function")
    # start
    asyncio.run(comm.start())
