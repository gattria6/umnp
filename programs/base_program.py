import sys

from umnp.microcontroller.communication.udp_communicator import UDPCommunicator
from umnp.microcontroller.devices.network.ethernet_w5500 import EthernetW5500
from umnp.microcontroller.devices.network.udp import UDPSender, UDPReceiver
from umnp.microcontroller.measurementdevice import MeasurementDevice
from umnp.microcontroller.tasks.periodictask import PeriodicTask
from umnp.proto.common import UDP_DATA_PORT, UDP_CMD_PORT

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import machine

    # noinspection PyUnresolvedReferences
    import ntptime

    # noinspection PyUnresolvedReferences
    import uasyncio as asyncio
else:
    from umnp.microcontroller.umock import machine
    import asyncio


def report_time(*args) -> str:
    return ""


def main():
    # configure network
    device = MeasurementDevice()
    spi = machine.SPI(
        0, 2_000_000, mosi=machine.Pin(19), miso=machine.Pin(16), sck=machine.Pin(18)
    )
    ether = EthernetW5500(
        spi, machine.Pin(17), machine.Pin(20), mac=device.generated_mac_raw(), dhcp=True
    )

    ntptime.settime()

    device.add_network_adapter(ether)
    sender = UDPSender(ether.ip, ether.netmask, UDP_DATA_PORT)
    receiver = UDPReceiver(ether.ip, UDP_CMD_PORT)

    communicator = UDPCommunicator(
        receiver=receiver, sender=sender, device_id=device.identifier
    )

    timer = PeriodicTask(report_time, print, 1000)
    communicator.add_task(task=timer, name="timer")
    # start
    asyncio.run(communicator.start())


if __name__ == "__main__":
    main()
