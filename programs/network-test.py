import sys

from umnp.devices import DEVICE_TYPE_TEST
from umnp.microcontroller.devices.network.ethernet_w5500 import EthernetW5500
from umnp.microcontroller.measurementdevice import MeasurementDevice
from umnp.microcontroller.tasks.periodictask import PeriodicTask
from umnp.proto.data_message import DataMessage

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import machine
    import gc

    # noinspection PyUnresolvedReferences
    import uasyncio as asyncio
else:
    from umnp.microcontroller.umock import machine

# SPI PARAMETERS
SPI_BAUD = 2_000_000
SPI_MOSI = machine.Pin(19)
SPI_MISO = machine.Pin(16)
SPI_SCK = machine.Pin(18)  # serial clock

# I2C PARAMETERS
I2C_SCL = machine.Pin(4)  # serial clock
I2C_SDA = machine.Pin(5)  # serial data

# ETHERNET PARAMETERS
ETH_CS = machine.Pin(17)  # chip select
ETH_RST = machine.Pin(20)  # reset

ETH_IP = "192.168.0.33"  # IP of microcontroller
ETH_SUBNET = "255.255.255.0"  # ...
ETH_GATEWAY = "192.168.0.1"  # ...
ETH_DNS = "192.168.0.2"  # technically not necessary
ETH_USE_DHCP = False


async def send(device: MeasurementDevice):
    comm = device.communicator

    data = "hello"

    if comm.network_error:
        await device.reset_network()

    if not comm.network_error:
        msg = DataMessage(data, device.identifier_raw, device.device_type)
        await comm.send_message(msg)
        msg = None

    gc.collect()


async def main():
    # configure network
    device = MeasurementDevice(device_type=DEVICE_TYPE_TEST)
    spi = device.add_spi(0, SPI_BAUD, mosi=SPI_MOSI, miso=SPI_MISO, sck=SPI_SCK)
    dev_mac = device.generated_mac_raw()
    ether = EthernetW5500(spi, ETH_CS, ETH_RST, dev_mac, ETH_USE_DHCP)
    ether.set_network(ETH_IP, ETH_SUBNET, ETH_GATEWAY, ETH_DNS)
    device.add_network_adapter(ether)

    comm = device.create_communicator()

    comm.add_task(
        PeriodicTask(send, None, 1000, device),
        "send",
    )
    comm.add_task(PeriodicTask(device.send_life_sign, None, 5000), "life_sign")

    asyncio.run(comm.start())


if __name__ == "__main__":
    asyncio.run(main())
