import sys

from umnp.microcontroller.communication.udp_communicator import UDPCommunicator
from umnp.microcontroller.devices.network.ethernet_w5500 import EthernetW5500
from umnp.microcontroller.devices.network.udp import UDPSender, UDPReceiver
from umnp.microcontroller.measurementdevice import MeasurementDevice
from umnp.microcontroller.sensors.lps28dfw import LPS28DFW
from umnp.microcontroller.sensors.sht45 import SHT45
from umnp.microcontroller.tasks.periodictask import PeriodicTask

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import machine

    # noinspection PyUnresolvedReferences
    import uasyncio as asyncio
else:
    from umnp.microcontroller.umock import machine
    import asyncio


def test_function(*args):
    print("test_function called with: ", *args)
    result = " - ".join(*args)
    print(f"test_function returns '{result}'")
    return result


def main():
    # configure network
    device = MeasurementDevice()
    spi = machine.SPI(
        0, 2_000_000, mosi=machine.Pin(19), miso=machine.Pin(16), sck=machine.Pin(18)
    )
    ether = EthernetW5500(
        spi, machine.Pin(17), machine.Pin(20), mac=device.generated_mac_raw(), dhcp=True
    )
    print(ether.mac)
    device.add_network_adapter(ether)

    i2c = machine.I2C(id=1, scl=machine.Pin(27), sda=machine.Pin(26))
    sht45 = SHT45(i2c)
    p_sensor = LPS28DFW(i2c)

    sender = UDPSender(ether.ip, ether.netmask, 7777)
    receiver = UDPReceiver(ether.ip, 7776)
    comm = UDPCommunicator(
        receiver=receiver, sender=sender, device_id=device.identifier
    )

    x = PeriodicTask(test_function, print, 1000)
    comm.add_task(x, "test_function")
    # start
    asyncio.run(comm.start())


if __name__ == "__main__":
    main()
