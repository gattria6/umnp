# This file is licensed under the Apache License, Version 2.0
# based on https://github.com/jbentham/pico/blob/main/rp_adc_test.py
# which is  Copyright (c) 2021 Jeremy P Bentham and licensed under the Apache License, Version 2.0

import array
import asyncio

import uctypes

import external.rp_devices as devs


class DmaAdc:
    def __init__(self, pin: int, sample_count: int = 1000, sampling_rate: int = 100000):
        self._pin_number = pin
        self._channel_number = pin - 26
        self._adc = devs.ADC_DEVICE
        self._pin = devs.GPIO_PINS[pin]
        self._pad = devs.PAD_PINS[pin]
        self._pin.GPIO_CTRL_REG = devs.GPIO_FUNC_NULL
        self._pad.PAD_REG = 0
        self._buffer = None
        self._n_samples = sample_count
        self.set_sample_count(sample_count)
        self._sampling_rate = sampling_rate
        self._dma_channel = devs.DMA_CHANS[self._channel_number]
        self._dma = devs.DMA_DEVICE
        self._ready = False
        self._lock = asyncio.Lock()

    def set_sample_count(self, count: int):
        self._n_samples = count
        self._buffer = array.array("H", (0 for _ in range(self._n_samples)))

    async def sample(self):
        with self._lock:
            self._ready = False
            self._adc.CS_REG = self._adc.FCS_REG = 0
            self._adc.CS.EN = 1
            self._adc.CS.AINSEL = self._channel_number
            self._adc.FCS.EN = self._adc.FCS.DREQ_EN = 1
            self._adc.DIV_REG = (48000000 // self._sampling_rate - 1) << 8
            self._adc.FCS.THRESH = self._adc.FCS.OVER = self._adc.FCS.UNDER = 1

            self._dma_channel.READ_ADDR_REG = devs.ADC_FIFO_ADDR
            self._dma_channel.WRITE_ADDR_REG = uctypes.addressof(self._buffer)
            self._dma_channel.TRANS_COUNT_REG = self._n_samples

            self._dma_channel.CTRL_TRIG_REG = 0
            self._dma_channel.CTRL_TRIG.CHAIN_TO = self._channel_number
            self._dma_channel.CTRL_TRIG.INCR_WRITE = (
                self._dma_channel.CTRL_TRIG.IRQ_QUIET
            ) = 1
            self._dma_channel.CTRL_TRIG.TREQ_SEL = devs.DREQ_ADC
            self._dma_channel.CTRL_TRIG.DATA_SIZE = 1
            self._dma_channel.CTRL_TRIG.EN = 1

            while self._adc.FCS.LEVEL:
                _ = self._adc.FIFO_REG

            self._adc.CS.START_MANY = 1
            while self._dma_channel.CTRL_TRIG.BUSY:
                await asyncio.sleep(10 / 1000)

            self._adc.CS.START_MANY = 0
            self._dma_channel.CTRL_TRIG.EN = 0
            self._ready = True
            return [("%1.3f" % (val * 3.3 / 4096)) for val in self._buffer]
