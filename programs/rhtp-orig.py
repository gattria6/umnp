import sys

from umnp.microcontroller.sensors.lps28dfw import LPS28DFW
from umnp.microcontroller.sensors.sht45 import SHT45

if sys.implementation.name == "micropython":
    # noinspection PyUnresolvedReferences
    import machine

    # noinspection PyUnresolvedReferences
    import uasyncio as asyncio
else:
    from umnp.microcontroller.umock import machine


async def main():
    i2c = machine.I2C(id=1, scl=machine.Pin(27), sda=machine.Pin(26))
    sht45 = SHT45(i2c)
    p_sensor = LPS28DFW(i2c, i2c_address=0x5C)
    while True:
        t, rh = await sht45.measure()
        p, p_t = await p_sensor.measure()

        print(f"temperature:     {t}")
        print(f"rH:              {rh}")
        print(f"pressure:        {p}")
        print(f"temperature (p): {p_t}")
        print()
        print()
        await asyncio.sleep(1)


if __name__ == "__main__":
    asyncio.run(main())
