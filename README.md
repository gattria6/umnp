# Unnamend measurement network project preparations

Please check the **[issue tracker](https://gitlab.phaidra.org/gattria6/unnamed-measurement-network-project-preparations/-/issues)** and the **[wiki](https://gitlab.phaidra.org/gattria6/unnamed-measurement-network-project-preparations/-/wikis/home)**.

This git repository contains the preparations for the yet unnamed measurement network project for 'Turmlabor'.

Once the initial specifications have been created and a name has been found, the project will be copied to the aerosol namespace. The idea is that we keep as much as the project in gitlab. To do we need to develop a suitable gitlab workflow. Using an external gitlab repository allows us to experiment without cluttering the repository history.


# Motivation
We want to software to acquire data and control experimental setups, both permanent and ad-hoc, e.g. for students. Instead of using a mixture of vendor software and custom tools that need to be adapted for each project, we create a single repository with a solution that can be reused and extended.

# System outline
We want to provide an easy to use toolkit to acquire data and control experimental setups. A setup consists of one or more devices (e.g. CPCs) and sensors (e.g. temperature, relative humidity, pressure).

The idea is, that each device will be connected to a (physically isolated) network (ethernet), and the software will automatically detect added devices and acquire data. To do so, we need several components

## Quality
Our main focus should be quality, QA, and documentation. All components should be traceable (software and hardware versions), and the software development should be test-driven.

## Microcontroller
Each device or sensor (or sensor cluster) will have its own microcontroller. This microcontroller will communicate control the device and broadcast acquired data. For this we need a microcontroller platform that is easy to extend, standardised and relatively cheap (so we can keep spares at hand).

See https://gitlab.phaidra.org/gattria6/unnamed-measurement-network-project-preparations/-/wikis/Hardware

## Network
In the first iteration we require a physically isolated network. Each measurement device broadcasts the data via UDP.


## Data acquisition and control
Because the data is broadcast, the use of redundant data acquisition devices is possible. 

In the simplest form data acquisition and control will run on the same computer/laptop controlling the experiment.


## Protocol
See https://gitlab.phaidra.org/gattria6/unnamed-measurement-network-project-preparations/-/wikis/Protocol

## Software
Ideally all software will be written in Python or MicroPython. The system should be open source and free software.


# History
Over the course of the last few years we have attempted similar projects, e.g. 'Allessteuerung'. These efforts have not been wasted, because the experiences (and code) live on and have been used to create the SABRE ControlBox, Ground Control + Major Tom, The Count, and similiar tools that are still in use.

# ACTRIS
For ACTRIS we will (and have to) use the provided software for data acquisition. This project will be used to feed data into the ACTRIS system for components that are not supported by ACTRIS.












